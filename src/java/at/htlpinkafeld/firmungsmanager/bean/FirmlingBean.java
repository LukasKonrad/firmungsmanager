/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.bean;

import at.htlpinkafeld.firmungsmanager.pojo.Person;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

/**
 *
 * @author Lukas
 */
public class FirmlingBean{
    private Person person;
    private int firmlingsId;
    private int score;
    public boolean archived;
    
    
    public FirmlingBean(){
        this.person = new Person(-1,"", "", "", "", "", "", "", LocalDate.of(1,1,0000));
        this.firmlingsId = -1;
        this.score =0;
        this.archived = false;
    }
    
    /**
     * Creates a new instance of FirmlingBean
     */
    public FirmlingBean(int firmlingsId, int score, boolean archived, int personId, String firstname, String lastname, String plz, String location, String address, String eMail, String password, LocalDate birthdate) {
        this.person = new Person(personId, firstname, lastname, plz, location, address, eMail, password, birthdate);
        this.firmlingsId = firmlingsId;
        this.score = score;
        this.archived = archived;
    }

    public int getFirmlingsId() {
        return firmlingsId;
    }

    public int getScore() {
        return score;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setFirmlingsId(int firmlingsId) {
        this.firmlingsId = firmlingsId;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
    
    
}
