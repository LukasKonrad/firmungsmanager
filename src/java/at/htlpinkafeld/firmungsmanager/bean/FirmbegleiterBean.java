/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.bean;

import at.htlpinkafeld.firmungsmanager.pojo.Person;
import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author Lukas
 */
public class FirmbegleiterBean{
    private Person person;
    private int fbId;
    private boolean isAdministrator;
    /**
     * Creates a new instance of FirmbegleiterBean
     */
    
    public FirmbegleiterBean(){
        
    }
    
    public FirmbegleiterBean(int fbId, boolean isAdministrator, int personId, String firstname, String lastname, String plz, String location, String address, String eMail, String password, LocalDate birthDate) {
        this.person = new Person(personId, firstname, lastname, plz, location, address, eMail, password, birthDate);
        this.fbId = fbId;
        this.isAdministrator = false;
    }

    public int getFbId() {
        return fbId;
    }

    public void setFbId(int fbId) {
        this.fbId = fbId;
    }

    public boolean isIsAdministrator() {
        return isAdministrator;
    }

    public void setIsAdministrator() {
        this.isAdministrator = !isAdministrator;
    }
    
    
}
