/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author Lukas
 */
public class Firmbegleiter extends Person{
    private int fbId;
    private boolean isAdministrator;

    public Firmbegleiter(int fbId, boolean isAdministrator, int personId, String firstname, String lastname, String plz, String location, String address, String eMail, String password, LocalDate birthdate) {
        super(personId, firstname, lastname, plz, location, address, eMail, password, birthdate);
        this.fbId = fbId;
        this.isAdministrator = isAdministrator;
    }

    public int getFbId() {
        return fbId;
    }

    public boolean isIsAdministrator() {
        return isAdministrator;
    }

    public void setFbId(int fbId) {
        this.fbId = fbId;
    }

    public void setIsAdministrator(boolean isAdministrator) {
        this.isAdministrator = isAdministrator;
    }
    
    
}
