/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

import java.util.List;

/**
 *
 * @author patri
 */
public class Modul {
    private String title;
    private List<Event> events;
    private int maxPoints;

    public Modul(String title, List<Event> events, int maxPoints) {
        this.title = title;
        this.events = events;
        this.maxPoints = maxPoints;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }    
}
