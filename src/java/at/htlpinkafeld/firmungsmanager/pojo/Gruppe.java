/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

/**
 *
 * @author Lukas
 */
public class Gruppe {
    private int gruppenID;
    private String name;
    private boolean isArchived;
    private int scoreMin;

    public Gruppe(int gruppenID, String name, boolean isArchived, int scoreMin) {
        this.gruppenID = gruppenID;
        this.name = name;
        this.isArchived = isArchived;
        this.scoreMin = scoreMin;
    }

    public int getGruppenID() {
        return gruppenID;
    }

    public String getName() {
        return name;
    }

    public boolean isIsArchived() {
        return isArchived;
    }

    public int getScoreMin() {
        return scoreMin;
    }

    public void setGruppenID(int gruppenID) {
        this.gruppenID = gruppenID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIsArchived(boolean isArchived) {
        this.isArchived = isArchived;
    }

    public void setScoreMin(int scoreMin) {
        this.scoreMin = scoreMin;
    }
    
    
}
