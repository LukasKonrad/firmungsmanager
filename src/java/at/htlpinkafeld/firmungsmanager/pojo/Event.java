/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lukas
 */
public class Event {
    private String title;
    private String description;
    private LocalDate begin;
    private LocalDate end;
    private List<Firmling> participants;
    private int maxParticipants;
    private Address place;

    public Event(String title, String description, LocalDate begin, LocalDate end, List<Firmling> participants, int maxParticipants, Address place) {
        this.title = title;
        this.description = description;
        this.begin = begin;
        this.end = end;
        this.participants = participants;
        this.maxParticipants = maxParticipants;
        this.place = place;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getBegin() {
        return begin;
    }

    public void setBegin(LocalDate begin) {
        this.begin = begin;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public List<Firmling> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Firmling> participants) {
        this.participants = participants;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public Address getPlace() {
        return place;
    }

    public void setPlace(Address place) {
        this.place = place;
    }
    
    public boolean isOpen(){
        if(this.maxParticipants >= this.participants.size() || this.begin.isAfter(LocalDate.now()))
            return false;
        return true;
    }
    
    
}
