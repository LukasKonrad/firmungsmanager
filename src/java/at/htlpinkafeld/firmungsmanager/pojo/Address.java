/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

/**
 *
 * @author Lukas
 */
public class Address {
    private int zip;
    private String street;
    private int housenumber;
    private String location;

    public Address(int zip, String street, int housenumber, String location) {
        this.zip = zip;
        this.street = street;
        this.housenumber = housenumber;
        this.location = location;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(int housenumber) {
        this.housenumber = housenumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
