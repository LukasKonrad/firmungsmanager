/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.pojo;

import java.util.Date;

/**
 *
 * @author Lukas
 */
public class Module {
    private int modulID;
    private String title;
    private Date begin;
    private Date end;
    private int scores;

    public Module(int modulID, String title, Date begin, Date end, int scores) {
        this.modulID = modulID;
        this.title = title;
        this.begin = begin;
        this.end = end;
        this.scores = scores;
    }

    public int getModulID() {
        return modulID;
    }

    public String getTitle() {
        return title;
    }

    public Date getBegin() {
        return begin;
    }

    public Date getEnd() {
        return end;
    }

    public int getScores() {
        return scores;
    }

    public void setModulID(int modulID) {
        this.modulID = modulID;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public void setScores(int scores) {
        this.scores = scores;
    }
    
    
}
