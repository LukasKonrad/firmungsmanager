/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.firmungsmanager.service;

import at.htlpinkafeld.firmungsmanager.pojo.Person;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author patri
 */


@ManagedBean("LogInService")
@SessionScoped
public class LogInService {

    private List<Person> persons;
    
    public LogInService() {
        persons = new ArrayList<Person>();
        persons.add(new Person(1234,"Patrick","Baldauf","7443","Feldgasse 41","Liebing","patrick.baldauf@htlpinkafeld.at","secret",LocalDate.of(2001, 6, 7)));
        persons.add(new Person(1235,"Simon","Wuenscher","7589","Hauptstraße 7","Großwillfersdorf","simon.wuenscher@htlpinkafeld.at","secret",LocalDate.of(2000, 7, 9)));
        persons.add(new Person(1236,"Lukas","Konrad","7954","Gartengasse 5","Neuberg-Bergen","lukas.konrad@htlpinkafeld.at","secret",LocalDate.of(2001, 6, 7)));
        persons.add(new Person(1237,"Max","Mustermann","1111","Hauptstraße 1","Pinkafeld","max.mustermann@htlpinkafeld.at","secret",LocalDate.of(2001, 1, 1)));
        
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }
    
    
}
