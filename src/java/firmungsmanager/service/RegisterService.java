/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firmungsmanager.service;

import firmungsmanager.pojo.Person;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lukas
 */
public class RegisterService {
    private List<Person> persList;

    public RegisterService(){
        this.persList = new ArrayList<>();
    }
    
    public RegisterService(List<Person> persList) {
        this.persList = new ArrayList<>();
    }
    
    public Object register(Person person){
        this.persList.add(person);
        return null;
    }
    
}
