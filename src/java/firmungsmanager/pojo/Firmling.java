/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firmungsmanager.pojo;

import java.util.Date;

/**
 *
 * @author Lukas
 */
public class Firmling extends Person{
    private int firmlingsId;
    private int score;
    public boolean archived;

    public Firmling(int firmlingsId, int score, boolean archived, int personId, String firstname, String lastname, String plz, String location, String address, String eMail, String password, Date birthdate) {
        super(personId, firstname, lastname, plz, location, address, eMail, password, birthdate);
        this.firmlingsId = firmlingsId;
        this.score = score;
        this.archived = archived;
    }  

    public int getFirmlingsId() {
        return firmlingsId;
    }

    public int getScore() {
        return score;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setFirmlingsId(int firmlingsId) {
        this.firmlingsId = firmlingsId;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }
    
    
}
