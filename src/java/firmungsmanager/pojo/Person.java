/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firmungsmanager.pojo;

import java.util.Date;

/**
 *
 * @author Lukas
 */
public class Person {
    private int personId;
    private String firstname;
    private String lastname;
    private String plz;
    private String location;
    private String address;
    private String mail;
    private String password;
    private Date birthdate;

    public Person(int personId, String firstname, String lastname, String plz, String location, String address, String eMail, String password, Date birthDate) {
        this.personId = personId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.plz = plz;
        this.location = location;
        this.address = address;
        this.mail = eMail;
        this.password = password;
        this.birthdate = birthdate;
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }

    public int getPersonId() {
        return personId;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPlz() {
        return plz;
    }

    public String getLocation() {
        return location;
    }

    public String getAddress() {
        return address;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Date getBirthdate() {
        return birthdate;
    }
    
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
}