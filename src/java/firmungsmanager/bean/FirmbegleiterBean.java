/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firmungsmanager.bean;

import firmungsmanager.pojo.Person;
import java.util.Date;

/**
 *
 * @author Lukas
 */
public class FirmbegleiterBean{
    private Person person;
    private int fbId;
    /**
     * Creates a new instance of FirmbegleiterBean
     */
    
    public FirmbegleiterBean(){
        
    }
    
    public FirmbegleiterBean(int fbId, boolean isAdministrator, int personId, String firstname, String lastname, String plz, String location, String address, String eMail, String password, Date birthDate) {
        this.person = new Person(personId, firstname, lastname, plz, location, address, eMail, password, birthDate);
        this.fbId = fbId;
    }

    public int getFbId() {
        return fbId;
    }

    public void setFbId(int fbId) {
        this.fbId = fbId;
    }
}
